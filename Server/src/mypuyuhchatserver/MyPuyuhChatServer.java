package mypuyuhchatserver;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
/**
 *
 * @author admin
 */
public class MyPuyuhChatServer {

    private static final List<ClientHandler> clients = Collections.synchronizedList(new ArrayList<>());

    public static void main(String[] args) {
        ServerSocket serverSocket;
        try {
            serverSocket = new ServerSocket(55555);
            System.out.println("Server started. Waiting for clients...");

            while (true) {
                Socket clientSocket = serverSocket.accept();
                System.out.println("Client connected: " + clientSocket);

                // Create a new thread to handle the client
                ClientHandler clientHandler = new ClientHandler(clientSocket);
                clients.add(clientHandler);
                new Thread(clientHandler).start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static class ClientHandler implements Runnable {
        private final Socket clientSocket;
        private final Scanner in;
        private final PrintWriter out;
        private String username;
        
        public ClientHandler(Socket socket) throws IOException {
            this.clientSocket = socket;
            this.in = new Scanner(socket.getInputStream());
            this.out = new PrintWriter(socket.getOutputStream(), true);
            
            setUsername();
        }

        @Override
        public void run() {
            try {
                while (true) {
                    if (in.hasNextLine()) {
                        String message = in.nextLine();
                        System.out.println("Received from client: " + message);

                        // Broadcast the message to all connected clients
                        broadcast(message, this);
                            // Echo the message back to the sender
                        //sendMessage("< "+getUsername()+" > " + message);
                        sendMessage(message);
                    }
                }
            } finally {
                try {
                    in.close();
                    out.close();
                    clientSocket.close();
                    clients.remove(this);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        private void broadcast(String message, ClientHandler sender) {
            synchronized (clients) {
                for (ClientHandler client : clients) {
                    if (client != sender) {
                        client.sendMessage(message);
                    }
                }
            }
            System.out.println("Broadcasted message to clients: " + message);
        }

        private void sendMessage(String message) {
            System.out.println("Sending message to client: " + message);
            out.println(message);
            out.flush(); // Manually flush the output stream
            System.out.println("Message sent to client: " + message);
        }
        private void setUsername() {
            // Retrieve the username from the client when it connects
            if (in.hasNextLine()) {
                this.username = in.nextLine();
                System.out.println("Username set for client: " + username);
            }
        }

        // Add a getUsername method to retrieve the username
        public String getUsername() {
            return username;
        }
    }
    
}
