package mypuyuhchatclient;

import java.security.SecureRandom;
import java.util.Base64;
import my.sainet.crypto.MyPuyuhNativeLibrary;

/**
 *
 * @author admin
 */
public class EncryptionUtility {
    private static final byte[] INPUT_KEY = { 
            0x6f, 0x6b, 0x68, 0x6f, 0x76, 0x62, 0x61, 0x79, 0x64, 0x6c, 0x66, 0x6b, 0x73, 0x68, 0x6d, 0x6c, 
            0x65, 0x7a, 0x63, 0x69, 0x61, 0x61, 0x6b, 0x71, 0x78, 0x67, 0x6a, 0x67, 0x64, 0x79, 0x6a, 0x6c
    };

    public static byte[] getRandomBytes() {
        int numBits = 256;
        int numBytes = (int) Math.ceil(numBits / 8.0);
        SecureRandom secureRandom = new SecureRandom();
        byte[] randomBytes = new byte[numBytes];
        secureRandom.nextBytes(randomBytes);
        return randomBytes;
    }

    public static String encryptMessage(String message) {
        byte[] plainTextBytes = message.getBytes();
        byte[] cipherTextBytes = new MyPuyuhNativeLibrary().EnkripMesej(INPUT_KEY, plainTextBytes, plainTextBytes.length);
        return Base64.getEncoder().encodeToString(cipherTextBytes);
    }

    public static String decryptMessage(String message) {
        byte[] cipherMessage = Base64.getDecoder().decode(message);
        byte[] plainTextBytes = new MyPuyuhNativeLibrary().DekripMesej(INPUT_KEY, cipherMessage, cipherMessage.length);
        return new String(plainTextBytes);
    }
    
    
}
