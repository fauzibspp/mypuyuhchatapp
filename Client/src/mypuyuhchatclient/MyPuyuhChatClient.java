package mypuyuhchatclient;

import javax.swing.*;
import javax.swing.text.DefaultCaret;
import java.awt.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;
import javax.swing.text.BadLocationException;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;
/**
 *
 * @author admin
 */
public class MyPuyuhChatClient extends JFrame {

    private JTextPane chatArea;
    private JTextField messageField;
    private JButton sendButton;
    private PrintWriter writer;
    private Scanner scanner;
    private String userName;

    public MyPuyuhChatClient() {
        // Prompt for user name
        userName = JOptionPane.showInputDialog("Enter your name:");
        
        setTitle("Chat Client - " + userName);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(400, 300);
        setLocationRelativeTo(null);

        chatArea = new JTextPane();
        chatArea.setContentType("text/html");
        chatArea.setEditable(false);
        
        DefaultCaret caret = (DefaultCaret) chatArea.getCaret();
        caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);

        messageField = new JTextField();
        messageField.addActionListener(e -> sendMessage());

        sendButton = new JButton("Send");
        sendButton.addActionListener(e -> sendMessage());

        setLayout(new BorderLayout());
        add(new JScrollPane(chatArea), BorderLayout.CENTER);
        JPanel bottomPanel = new JPanel(new BorderLayout());
        bottomPanel.add(messageField, BorderLayout.CENTER);
        bottomPanel.add(sendButton, BorderLayout.EAST);
        add(bottomPanel, BorderLayout.SOUTH);

        try {
            System.out.println("Connecting to server...");
            Socket socket = new Socket(Configuration.SERVER_HOST, Configuration.SERVER_PORT);
            System.out.println("Connected to server");

            // Send the user name to the server
            writer = new PrintWriter(socket.getOutputStream(), true);
            writer.println(userName);
            
            scanner = new Scanner(socket.getInputStream());
            new Thread(this::listenToServer).start();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    private void listenToServer() {
        try {
            while (scanner.hasNextLine()) {
                String receivedMessage = scanner.nextLine();
                handleReceivedMessage(receivedMessage);
            }
        } catch (Exception e) {
            // Handle the exception appropriately (log or show an error message)
            e.printStackTrace();
        } finally {
            // Close resources in the end
            closeResources();
        }
    }
    
    private void closeResources() {
        try {
            if (writer != null) {
                writer.close();
            }
            if (scanner != null) {
                scanner.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
       
    private void handleReceivedMessage(String receivedMessage) {
        String[] parts = receivedMessage.split("\\|\\|", 2);
        if (parts.length == 2) {
            String user = parts[0];
            String encryptedMessage = parts[1];
            String decryptedMessage = EncryptionUtility.decryptMessage(encryptedMessage);
            System.out.println("Received decrypted message from " + user + ": " + decryptedMessage);
            appendMessageToChatArea(user, decryptedMessage, false);
        } else {
            System.out.println("Invalid message format received from the server: " + receivedMessage);
        }
    }
    
    private void appendMessageToChatArea(String user, String message, boolean sent) {
        SwingUtilities.invokeLater(() -> {
            String formattedMessage = formatMessage(user, message, sent);
            if (sent) {
                appendToPane(chatArea, formattedMessage, "right");
            } else {
                appendToPane(chatArea, formattedMessage, "left");
            }
            chatArea.setCaretPosition(chatArea.getDocument().getLength());
        });
    }

    private void appendToPane(JTextPane textPane, String msg, String align) {
        HTMLDocument doc = (HTMLDocument) textPane.getDocument();
        HTMLEditorKit editorKit = (HTMLEditorKit) textPane.getEditorKit();
        try {
            if (align.equals("right")) {
                editorKit.insertHTML(doc, doc.getLength(), "<div style='color: black; text-align: right;'>" + msg + "</div>", 0, 0, null);
            } else {
                editorKit.insertHTML(doc, doc.getLength(), "<div style='color: black; text-align: left;'>" + msg + "</div>", 0, 0, null);
            }
        } catch (BadLocationException | IOException e) {
            e.printStackTrace();
        }
    }


    private String formatMessage(String user, String message, boolean sent) {
        String style = sent ? "color: blue; text-align: right;" : "color: green; text-align: left;";
        return "<div style='" + style + "'><b>" + user + ": </b>" + message + "</div>";
    }
  
    private void sendMessage() {
        String message = messageField.getText();

        if (!message.isEmpty()) {
            try {
                String encryptedMessage = EncryptionUtility.encryptMessage(message);
                String fullMessage = userName + "||" + encryptedMessage;
                writer.println(fullMessage);
                messageField.setText("");
                System.out.println("< " + userName + " > sent encrypted message to server: " + encryptedMessage);
                appendMessageToChatArea(userName, message, true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> new MyPuyuhChatClient().setVisible(true));
    }
    
}
